import { API_BASE, handleUnknownStatus } from "./base.ts";
import type { ApiResponse } from "./base";
import type { Bill, BillErrors } from "../models";

type BillApiResponse = ApiResponse<Bill, BillErrors>;
type BillsApiResponse = ApiResponse<Bill[], BillErrors>;

const API_BILL_BASE: string = API_BASE + "bill/";

export async function getBills(): Promise<BillsApiResponse> {
    const url: string = API_BILL_BASE;
    const response = await fetch(url, {
        method: "GET",
    });

    if (!response.ok) {
        await handleUnknownStatus(response);
    }

    const bills: Bill[] = await response.json();
    return { data: bills };
}

export async function getBillById(id: string): Promise<BillApiResponse> {
    const url: string = API_BILL_BASE + `${id}/`;
    const response = await fetch(url, {
        method: "GET",
    });

    if (!response.ok) {
        await handleUnknownStatus(response);
    }

    const bill: Bill = await response.json();
    return { data: bill };
}

export async function createBill(bill: Bill): Promise<BillApiResponse> {
    const url: string = API_BILL_BASE;
    const body: string = JSON.stringify(bill);

    const response = await fetch(url, {
        method: "POST",
        body: body,
        headers: {
            "Content-Type": "application/json",
        },
    });

    if (response.status === 400) {
        const errors: BillErrors = await response.json();
        return { errors: errors };
    }

    if (!response.ok) {
        await handleUnknownStatus(response);
    }

    const newBill: Bill = await response.json();
    return { data: newBill };
}

export async function deleteBill(bill: Bill): Promise<void> {
    const url: string = API_BILL_BASE + `${bill.id}/`;
    const response = await fetch(url, {
        method: "DELETE",
    });

    if (!response.ok) {
        await handleUnknownStatus(response);
    }
}

export async function updateBill(bill: Bill): Promise<BillApiResponse> {
    const url: string = API_BILL_BASE + `${bill.id}/`;
    const body: string = JSON.stringify(bill);

    const response = await fetch(url, {
        method: "PUT",
        body: body,
        headers: {
            "Content-Type": "application/json",
        },
    });

    // User made an error
    if (response.status === 400) {
        const errors: BillErrors = await response.json();
        return { errors: errors };
    }

    if (!response.ok) {
        await handleUnknownStatus(response);
    }

    const updatedBill: Bill = await response.json();
    return { data: updatedBill };
}

export default {
    getBills,
    getBillById,
    createBill,
    deleteBill,
    updateBill,
}
