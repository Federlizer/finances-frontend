import { API_BASE, handleUnknownStatus } from "./base.ts";
import type { ApiResponse } from "./base";
import type { Category, CategoryErrors } from "../models";

type CategoryApiResponse = ApiResponse<Category, CategoryErrors>;
type CategoriesApiResponse = ApiResponse<Category[], CategoryErrors>;

const API_CATEGORY_BASE: string = API_BASE + "category/";

export async function getCategories(): Promise<CategoriesApiResponse> {
    const url: string = API_CATEGORY_BASE;

    const response = await fetch(url, {
        method: "GET",
    });

    if (!response.ok) {
        // Throws
        await handleUnknownStatus(response);
    }

    const categories: Category[] = await response.json();
    return { data: categories };
}

export async function getCategoryById(id: string): Promise<CategoryApiResponse> {
    const url: string = API_CATEGORY_BASE + `${id}/`;
    const response = await fetch(url, {
        method: "GET",
    });

    if (!response.ok) {
        // Throws
        await handleUnknownStatus(response);
    }

    const category: Category = await response.json();
    return { data: category };
}

export async function createCategory(category: Category): Promise<CategoryApiResponse> {
    const url: string = API_CATEGORY_BASE;
    const body: string = JSON.stringify(category);

    const response = await fetch(url, {
        method: "POST",
        body: body,
        headers: {
            "Content-Type": "application/json",
        },
    });

    // User made an error
    if (response.status === 400) {
        const errors: CategoryErrors = await response.json();
        return { errors: errors };
    }

    if (!response.ok) {
        await handleUnknownStatus(response);
    }

    const newCategory: Category = await response.json();
    return { data: newCategory };
}

export async function deleteCategory(category: Category): Promise<void> {
    const url: string = `${API_CATEGORY_BASE}${category.id}/`;
    const response = await fetch(url, {
        method: "DELETE",
    });

    if (!response.ok) {
        await handleUnknownStatus(response);
    }
}

export async function updateCategory(category: Category): Promise<CategoryApiResponse> {
    const url: string = `${API_CATEGORY_BASE}${category.id}/`;
    const body: string = JSON.stringify(category);

    const response = await fetch(url, {
        method: "PUT",
        body: body,
        headers: {
            "Content-Type": "application/json",
        },
    });

    // User made an error
    if (response.status === 400) {
        const errors: CategoryErrors = await response.json();
        return { errors: errors };
    }

    if (!response.ok) {
        await handleUnknownStatus(response);
    }

    const newCategory: Category = await response.json();
    return { data: newCategory };
}

export default {
    getCategories,
    getCategoryById,
    createCategory,
    deleteCategory,
    updateCategory,
};
