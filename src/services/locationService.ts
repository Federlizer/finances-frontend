import { API_BASE, handleUnknownStatus } from "./base.ts";
import type { ApiResponse } from "./base";
import type { Location, LocationErrors } from "../models";

type LocationApiResponse = ApiResponse<Location, LocationErrors>;
type LocationsApiResponse = ApiResponse<Location[], LocationErrors>;

const API_LOCATION_BASE: string = API_BASE + "location/";

export async function getLocations(): Promise<LocationsApiResponse> {
    const url: string = API_LOCATION_BASE;
    const response = await fetch(url, {
        method: "GET",
    });

    if (!response.ok) {
        await handleUnknownStatus(response);
    }

    const locations: Location[] = await response.json();
    return { data: locations };
}

export async function getLocationById(id: string): Promise<LocationApiResponse> {
    const url: string = API_LOCATION_BASE + `${id}/`;
    const response = await fetch(url, {
        method: "GET",
    });

    if (!response.ok) {
        await handleUnknownStatus(response);
    }

    const location: Location = await response.json();
    return { data: location };
}

export async function createLocation(location: Location): Promise<LocationApiResponse> {
    const url: string = API_LOCATION_BASE;
    const body: string = JSON.stringify(location);
    const response = await fetch(url, {
        method: "POST",
        body: body,
        headers: {
            "Content-Type": "application/json",
        },
    });

    if (response.status === 400) {
        const errors: LocationErrors = await response.json();
        return { errors: errors };
    }

    if (!response.ok) {
        await handleUnknownStatus(response);
    }

    const newLocation: Location = await response.json();
    return { data: newLocation };
}

export async function deleteLocation(location: Location): Promise<void> {
    const url: string = API_LOCATION_BASE + `${location.id}/`;
    const response = await fetch(url, {
        method: "DELETE",
    });

    if (!response.ok) {
        await handleUnknownStatus(response);
    }
}

export async function updateLocation(location: Location): Promise<LocationApiResponse> {
    const url: string = API_LOCATION_BASE + `${location.id}/`;
    const body: string = JSON.stringify(location);
    const response = await fetch(url, {
        method: "PUT",
        body: body,
        headers: {
            "Content-Type": "application/json",
        },
    });

    // User made an error
    if (response.status === 400) {
        const errors: LocationErrors = await response.json();
        return { errors: errors };
    }

    if (!response.ok) {
        await handleUnknownStatus(response);
    }

    const updatedLocation: Location = await response.json();
    return { data: updatedLocation };
}

export default {
    getLocations,
    getLocationById,
    createLocation,
    deleteLocation,
    updateLocation,
}
