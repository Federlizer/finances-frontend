import { API_BASE } from "./base";
import type { ApiResponse } from "./base";
import type { Currency, CurrencyErrors } from "../models";

type CurrencyApiResponse = ApiResponse<Currency, CurrencyErrors>;
type CurrenciesApiResponse = ApiResponse<Currency[], CurrencyErrors>;

const API_CURRENCY_BASE: string = API_BASE + "currency/";

export async function getCurrencies(): Promise<CurrenciesApiResponse> {
    const url: string = API_CURRENCY_BASE;

    const response = await fetch(url, {
        method: "GET",
    });

    if (!response.ok) {
        const text = await response.text();
        throw new Error(`${response.status} status from ${url}: ${text}`);
    }

    const currencies: Currency[] = await response.json();
    return { data: currencies };
};

export async function getCurrencyById(id: string): Promise<CurrencyApiResponse> {
    const url: string = API_CURRENCY_BASE + `${id}/`;

    const response = await fetch(url, {
        method: "GET",
    });

    if (!response.ok) {
        const text: string = await response.text();
        throw new Error(`${response.status} status from ${url}: ${text}`);
    }

    const currency: Currency = await response.json();
    return { data: currency };
}

/* Create a new currency
 * Returns an object with a `data` and `errors` keys. If the `errors` key
 * is undefined, the `data` object is fine to inspect.
 */
export async function createCurrency(currency: Currency): Promise<CurrencyApiResponse> {
    const url: string = API_CURRENCY_BASE;
    const body: string = JSON.stringify(currency);

    const response = await fetch(url, {
        method: "POST",
        body: body,
        headers: {
            "Content-Type": "application/json",
        },
    });

    // User made an error
    if (response.status === 400) {
        const errors: CurrencyErrors = await response.json();
        return { errors: errors };
    }

    if (!response.ok) {
        const text: string = await response.text();
        throw new Error(`${response.status} status from ${url}: ${text}`);
    }

    const newCurrency: Currency = await response.json();
    return { data: newCurrency };
}

export async function deleteCurrency(id: string): Promise<void> {
    const url: string = API_CURRENCY_BASE + `${id}/`;
    
    const response = await fetch(url, {
        method: "DELETE",
    });

    if (!response.ok) {
        const text: string = await response.text();
        throw new Error(`${response.status} status from ${url}: ${text}`);
    }
}

export async function updateCurrency(currency: Currency): Promise<CurrencyApiResponse> {
    const url: string = API_CURRENCY_BASE + `${currency.id}/`;
    const body: string = JSON.stringify(currency);
    const response = await fetch(url, {
        method: "PUT",
        body: body,
        headers: {
            "Content-Type": "application/json",
        },
    });

    if (response.status === 400) {
        const errors: CurrencyErrors = await response.json();
        return { errors: errors };
    }

    if (!response.ok) {
        const text: string = await response.text();
        throw new Error(`${response.status} status from ${url}: ${text}`);
    }

    const updatedCurrency: Currency = await response.json();
    return { data: updatedCurrency };
}

export default {
    getCurrencies,
    getCurrencyById,
    createCurrency,
    deleteCurrency,
    updateCurrency,
};
