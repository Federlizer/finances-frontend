const baseAddr = window.location.origin;

export const API_BASE = `${baseAddr}/api/`;

export async function handleUnknownStatus(response: Response) {
    const text = await response.text();
    throw new Error(`${response.status} status from ${response.url}: ${text}`);
}

export type ApiResponse<T, E> =
    | { data: T, errors?: undefined }
    | { data?: undefined, errors: E };
