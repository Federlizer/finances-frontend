import { createRouter, createWebHistory } from "vue-router"
import HomeView from "../views/HomeView.vue"

import BillsView from "../views/BillsView.vue";
import CategoriesView from "../views/CategoriesView.vue";
import CurrenciesView from "../views/CurrenciesView.vue";
import LocationsView from "../views/LocationsView.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    { path: "/", name: "home", component: HomeView },

    { path: "/bill", name: "bill:list", component: BillsView },
    { path: "/category", name: "category:list", component: CategoriesView },
    { path: "/currency", name: "currency:list", component: CurrenciesView },
    { path: "/location", name: "location:list", component: LocationsView },
  ],
})

export default router
