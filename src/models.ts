export interface Currency {
    id: string
    name: string
    code: string
    symbol: string
}

export interface CurrencyErrors {
    name?: string[]
    code?: string[]
    symbol?: string[]
}

export interface Category {
    id: string
    name: string
}

export interface CategoryErrors {
    name?: string[]
}

export interface Location {
    id: string
    name: string
}

export interface LocationErrors {
    name?: string[]
}

export interface Entry {
    id: string
    category: Category
    amount: number
}

export interface Bill {
    id: string
    date: string
    notes: string
    currency: Currency
    location: Location
    entries: Entry[]
}

export interface BillErrors {
    date?: string[]
}
