# finances-frontend

The frontend application for the [finances backend project](https://gitlab.com/Federlizer/finances).

- [x] Use tailwindcss
- [ ] Get rid of unnecessary configuration
    - [ ] Fix prettier (autoformatter)
    - [ ] Fix tsconfig (typechecking)
    - [ ] Fix eslint   (code quality checker)
- [ ] Would it be possible to get rid of the vite package as a whole or nah?
    - [ ] It looks like vite is the server that's running to serve the development
          pages. But if possible, it would be nicer to get rid of it and use the
          plain one, unless it's understood and useful.
- [ ] Get testing going (unit test, component test, but no E2E testing *yet*)


- [ ] MOBILE FIRST!
- [ ] Implement basic navigation
    - [x] Define routes and components
    - [x] Implement navigation menu
    - [ ] Implement Currency, Location, Category, Bill views (list all/view one)
    - [ ] Implement a 404 page
    - [ ] Properly react to route changes in reused components (https://router.vuejs.org/guide/essentials/dynamic-matching.html#Reacting-to-Params-Changes)
    - [ ] Use proper regexes to match only UUIDs in update views/routes
- [ ] Implement nice styling of components and views (404 page too)
- [ ] Create tests
- [ ] Connect frontend to backend
- [ ] Handle unavailable backend server gracefully


## Type Support for `.vue` Imports in TS

TypeScript cannot handle type information for `.vue` imports by default, so we replace the `tsc` CLI with `vue-tsc` for type checking. In editors, we need [Volar](https://marketplace.visualstudio.com/items?itemName=Vue.volar) to make the TypeScript language service aware of `.vue` types.

## Customize configuration

See [Vite Configuration Reference](https://vitejs.dev/config/).

## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Type-Check, Compile and Minify for Production

```sh
npm run build
```

### Run Unit Tests with [Vitest](https://vitest.dev/)

```sh
npm run test:unit
```

### Lint with [ESLint](https://eslint.org/)

```sh
npm run lint
```
